//
//  AppDelegate.swift
//  AppierSampleApp
//
//  Created by Appier on 2020/2/18.
//  Copyright © 2020 appier. All rights reserved.
//

import UIKit
import UserNotifications

fileprivate let appId:String = "AppId"
fileprivate let appGroup:String = "AppGroupName"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
     func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
         // Override point for customization after application launch
         let QG = QGSdk.getSharedInstance()
         #if DEBUG
           QG.onStart(appId, withAppGroup:appGroup, setDevProfile: true)
           print ("IN DEBUG")
         #else
           QG.onStart(appId, withAppGroup:appGroup, setDevProfile: false)
           print ("IN ELSE")
         #endif

        if #available(iOS 10.0, *) {
          let center = UNUserNotificationCenter.current()
          center.delegate = self
          center.requestAuthorization(options: [.badge, .carPlay, .alert, .sound]) { (granted, error) in
              print("Granted: \(granted), Error: \(String(describing: error))")
          }
        } else {
          // Fallback on earlier versions
          let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          UIApplication.shared.registerUserNotificationSettings(settings)
        }

        
        if #available(iOS 10.0, *) {
              let center = UNUserNotificationCenter.current()
              center.delegate = self
              QGSdk.setCarouselNotificationCategoryWithNextButtonTitle("Play", withOpenAppButtonTitle: "Checkout")
              center.requestAuthorization(options: [.badge, .carPlay, .alert, .sound]) { (granted, error) in
                print("Granted: \(granted), Error: \(String(describing: error))")
              }
        }
        
        if #available(iOS 10.0, *) {
              let center = UNUserNotificationCenter.current()
              center.delegate = self
              
              var categories: Set<UNNotificationCategory> = Set.init()
              categories.insert(QGSdk.getQGSliderPushActionCategory(withNextButtonTitle: ">> Next >>", withOpenAppButtonTitle: "Interested"))
              center.setNotificationCategories(categories)
              
              center.requestAuthorization(options: [.badge, .carPlay, .alert, .sound]) { (granted, error) in
                print("Granted: \(granted), Error: \(String(describing: error))")
              }
        }
    
        QG.getRecommendationForModelUserToProduct(completion: { (response) in
          print("Recommendation: \(response)")
        })
        
         return true
       }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      let QG = QGSdk.getSharedInstance()
      print("My token is: \(deviceToken.description)")
      QG.setToken(deviceToken as Data)
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
      print("Failed to get token, error: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      let QG = QGSdk.getSharedInstance()
      // to enable track click on notification
      QG.application(application, didReceiveRemoteNotification: userInfo)
      completionHandler(UIBackgroundFetchResult.noData)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
      QGSdk.getSharedInstance().userNotificationCenter(center, willPresent: notification)
      completionHandler([.alert, .badge, .sound]);
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler:@escaping() -> Void) {
      QGSdk.getSharedInstance().userNotificationCenter(center, didReceive: response)
      completionHandler()
    }

}


